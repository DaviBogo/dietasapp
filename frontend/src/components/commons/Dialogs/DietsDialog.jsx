import React, { Component } from "react";
import { Typography, Button, Slide, Grid, Dialog, DialogTitle, DialogContent, DialogActions, List, ListItem } from "@material-ui/core";

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

class DietsDialog extends Component {

    handleChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    render() {
        const { open, dieta, onClose } = this.props;
        console.log(dieta)

        return (
            <div>
                <Dialog
                    onClose={onClose}
                    aria-labelledby="customized-dialog-title"
                    open={open}
                    TransitionComponent={Transition}
                >
                    <DialogTitle onClose={onClose}>
                    </DialogTitle>
                    <DialogContent>
                        <Grid container>
                            {dieta === "" || dieta === null? (
                                <Typography> Nenhuma dieta encontrada. </Typography>
                            ) :
                                <Grid item xs={12}>
                                    <List>
                                        <ListItem>
                                            <Typography> Alimentos: {dieta.alimentos} </Typography>
                                        </ListItem>
                                        <ListItem>
                                            <Typography> Observações: {dieta.observacoes} </Typography>
                                        </ListItem>
                                    </List>
                                </Grid>
                            }
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={onClose} variant="contained" color="secondary" size="small">
                            Fechar
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}
export default DietsDialog;