import React, { Component } from "react";
import { Typography, Button, Slide, Grid, Dialog, TextField, DialogTitle, DialogContent, DialogActions } from "@material-ui/core";
import axios from "axios";

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

class SendDiet extends Component {

    state = {
        alimentos: "",
        observacoes: ""
    }

    createDiet = async (idMeta) => {

        console.log(idMeta)
        let meta = {
            id: idMeta.idMeta
        }
        console.log(meta)
        let dieta = {
            alimentos: this.state.alimentos,
            observacoes: this.state.observacoes
        }
        const formData = new FormData();
        formData.set("dieta", JSON.stringify(dieta))
        formData.set("meta", JSON.stringify(meta))
        const response =
            await
                axios.post('http://localhost:9090/api/dieta/cadastro', formData)
                    .then(function (response) {
                        if (response.status === 200) {
                            alert("Dieta criada com sucesso!");
                        } else {
                            alert(response.message);
                        }
                    });
    };

    handleChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    render() {
        const { open, metaId, onClose } = this.props;

        return (
            <div>
                <Dialog
                    onClose={onClose}
                    aria-labelledby="customized-dialog-title"
                    open={open}
                    TransitionComponent={Transition}
                >
                    <DialogTitle onClose={onClose}>
                    </DialogTitle>
                    <DialogContent>
                        <Grid container>
                            <Grid item xs={12}>
                                <Typography
                                    variant="h5"
                                    style={{ textAlign: "center", padding: 15 }}
                                >
                                    Sugerir Dieta
                                </Typography>
                            </Grid>
                            <TextField
                                label="Alimentos"
                                name="alimentos"
                                value={this.state.alimentos}
                                variant="outlined"
                                fullWidth
                                onChange={e => this.handleChange(e)}
                                margin="dense"
                                style={{ marginTop: 10 }} />
                            <TextField
                                label="Observações"
                                name="observacoes"
                                value={this.state.observacoes}
                                variant="outlined"
                                fullWidth
                                onChange={e => this.handleChange(e)}
                                margin="dense"
                                style={{ marginTop: 10 }} />
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => this.createDiet({ idMeta: metaId })} variant="outlined" color="secondary" size="small">
                            Enviar Dieta
                        </Button>
                        <Button onClick={onClose} variant="contained" color="secondary" size="small">
                            Fechar
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}
export default SendDiet;