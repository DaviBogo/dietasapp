import React, { Component } from "react";
import { Typography, Button, TextField, Grid, Card, Avatar, Select, FormControl, InputLabel, MenuItem } from "@material-ui/core";
import { Link } from "react-router-dom";
import history from "../../../history";
import axios from "axios";
import { Refresh } from "@material-ui/icons";

class UserPerfil extends Component {

    state = {
        id: localStorage.getItem("userId"),
        nome: "",
        email: "",
        sexo: "",
        peso: "",
        altura: "",
        limitacoes: "",
        sexoList: [
            "Masculino",
            "Feminino",
        ],
        role: ""
    }

    async componentDidMount() {
        const response =
            await
                axios.get(`http://localhost:9090/api/usuario/${localStorage.getItem("userId")}`);
        this.setState({
            nome: response.data.nome,
            email: response.data.email,
            sexo: response.data.sexo,
            peso: response.data.peso,
            altura: response.data.altura,
            limitacoes: response.data.limitacoes,
            role: response.data.role
        })
    };

    handleChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    editPerfil = async () => {

        let usuario = {
            id: this.state.id,
            nome: this.state.nome,
            sexo: this.state.sexo,
            email: this.state.email,
            peso: this.state.peso,
            altura: this.state.altura,
            limitacoes: this.state.limitacoes
        }
        const formData = new FormData();
        formData.set("usuario", JSON.stringify(usuario))
        const response =
            await
                axios.put('http://localhost:9090/api/usuario', formData)
                    .then(function (response) {
                        if (response.status === 200) {
                            alert("Usuário editado com sucesso!");
                        } else {
                            alert(response.message);
                        }
                    });
    };

    turnNutritionist = async () => {
        let id = this.state.id
        const formData = new FormData();
        formData.set("id_usuario", id)
        const response =
            await
                axios.put('http://localhost:9090/api/usuario/upgrade/nutricionista', formData)
                    .then(function (response) {
                        if (response.status === 200) {
                            alert("Upgrade para nutricionista realizado com sucesso!");
                        } else {
                            alert(response.message);
                        }
                    });
    }

    render() {
        return (
            <div style={{ backgroundColor: "#fed766", height: "47.1em" }}>
                <Grid container justify="center">
                    <Grid item xs={12} md={4}>
                        <Card style={{ marginTop: 100, padding: 10 }}>
                            <Grid container justify="center">
                                <Grid container item xs={6} style={{ marginTop: 10 }} alignItems="flex-start" justify="center">
                                    <Avatar style={{ height: 190, width: 190, backgroundColor: "#ffd3b6" }}>
                                        <h1>
                                            {this.state.nome.charAt(0).toUpperCase()}
                                        </h1>
                                    </Avatar>
                                    <Button
                                        color="secondary"
                                        variant="contained"
                                        disabled={false}
                                        onClick={() => this.editPerfil()}
                                    >
                                        Editar Perfil
                                     </Button>
                                </Grid>
                                <Grid item xs={6}>
                                    <TextField
                                        label="Nome"
                                        name="nome"
                                        value={this.state.nome || ""}
                                        variant="outlined"
                                        fullWidth
                                        onChange={e => this.handleChange(e)}
                                        margin="dense"
                                        style={{ marginTop: 10 }} />
                                    <TextField
                                        label="Email"
                                        name="email"
                                        value={this.state.email || ""}
                                        variant="outlined"
                                        fullWidth
                                        onChange={e => this.handleChange(e)}
                                        margin="dense"
                                        style={{ marginTop: 10 }} />
                                    <TextField

                                        id="outlined-select-currency"
                                        select
                                        label="Sexo"
                                        name="sexo"
                                        value={this.state.sexo || ""}
                                        fullWidth
                                        onChange={e => this.handleChange(e)}
                                        variant="outlined"
                                        margin="dense"
                                        style={{ marginTop: 10 }}
                                    >
                                        {this.state.sexoList.map((option) => (
                                            <MenuItem key={option} value={option}>
                                                {option}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                    <TextField
                                        label="Peso (kg)"
                                        name="peso"
                                        value={this.state.peso || ""}
                                        variant="outlined"
                                        fullWidth
                                        onChange={e => this.handleChange(e)}
                                        margin="dense"
                                        style={{ marginTop: 10 }} />
                                    <TextField
                                        label="Altura (m)"
                                        name="altura"
                                        value={this.state.altura || ""}
                                        variant="outlined"
                                        fullWidth
                                        onChange={e => this.handleChange(e)}
                                        margin="dense"
                                        style={{ marginTop: 10 }} />
                                    <TextField
                                        label="Limitações"
                                        name="limitacoes"
                                        value={this.state.limitacoes || ""}
                                        variant="outlined"
                                        fullWidth
                                        onChange={e => this.handleChange(e)}
                                        margin="dense"
                                        style={{ marginTop: 10 }} />
                                </Grid>
                            </Grid>
                            <Grid container item xs={12} justify="space-evenly">
                                <Link to="/meta/criar" style={{ textDecoration: "none" }}>
                                    <Button
                                        color="secondary"
                                        variant="outlined"
                                        size="medium"
                                        onClick={() => { }}
                                        style={{ marginTop: 10 }}
                                    >
                                        Criar Nova Meta
                                </Button>
                                </Link>
                                <Link to="/metas/dietas" style={{ textDecoration: "none" }}>
                                    <Button
                                        color="secondary"
                                        variant="outlined"
                                        size="medium"
                                        onClick={() => { }}
                                        style={{ marginTop: 10 }}
                                    >
                                        Minhas metas
                                        </Button>
                                </Link>
                                {this.state.role === "nutricionista" ? (
                                    <Link to="/metas/visualizar" style={{ textDecoration: "none" }} >
                                        <Button
                                            color="secondary"
                                            variant="outlined"
                                            size="medium"
                                            style={{ marginTop: 10 }}
                                        >
                                            Visualizar Metas
                                            </Button>
                                    </Link>
                                ) :
                                    <Button
                                        color="secondary"
                                        variant="outlined"
                                        size="medium"
                                        onClick={() => this.turnNutritionist()}
                                        style={{ marginTop: 10 }}
                                    >
                                        Virar Nutricionista
                                </Button>}

                            </Grid>
                        </Card>
                    </Grid>
                </Grid>
            </div>
        )
    }
}

export default UserPerfil;