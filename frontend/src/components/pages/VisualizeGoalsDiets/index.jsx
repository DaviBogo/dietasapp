import React, { Component } from "react";
import { Typography, Grid, Card, Accordion, AccordionSummary, Button, AccordionDetails, List, ListItem } from "@material-ui/core";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Link } from "react-router-dom";
import history from "../../../history";
import DietsDialog from "../../commons/Dialogs/DietsDialog"
import axios from "axios";

class VisualizeGoal extends Component {

    state = {
        sendDietDialog: false,
        metas: [],
        dieta: ""
    }

    async componentDidMount() {
        const response =
            await
                axios.get(`http://localhost:9090/api/metas/usuario/${localStorage.getItem("userId")}`);
        this.setState({
            metas: response.data
        })
    };

    render() {
        return (
            <div style={{ backgroundColor: "#fed766", height: "47.1em" }}>
                <Grid container justify="center">
                    <Grid item xs={12} md={4}>
                        <Card style={{ marginTop: 100, padding: 10 }}>
                            <Typography
                                variant="h5"
                                style={{ textAlign: "center", padding: 15 }}
                            >
                                Minhas Metas
                            </Typography>
                            {this.state.metas.map((meta) =>
                                <Accordion variant="outlined" expanded={true}>
                                    <AccordionDetails>
                                        <Grid container justify="space-between">
                                            <Grid item xs={8}>
                                                <List>
                                                    <ListItem>
                                                        <Typography> Peso desejado: {meta.pesoDesejado} </Typography>
                                                    </ListItem>
                                                    <ListItem>
                                                        <Typography> Tempo desejado: {meta.tempoDesejado} </Typography>
                                                    </ListItem>
                                                    <ListItem>
                                                        <Typography> Observações: {meta.observacoes} </Typography>
                                                    </ListItem>
                                                </List>
                                            </Grid>
                                            <Grid container item xs={2} alignItems="flex-end" justify="flex-end" xs={4}>
                                                <Button
                                                    size="small"
                                                    color="secondary"
                                                    variant="contained"
                                                    onClick={() => this.setState({ sendDietDialog: !this.state.sendDietDialog, dieta: meta.dieta})}
                                                >
                                                    Visualizar Dieta
                                            </Button>
                                            </Grid>
                                        </Grid>
                                    </AccordionDetails>
                                </Accordion>
                            )}
                            <Grid container item xs={12} alignItems="center">
                                <Button
                                    size="small"
                                    color="secondary"
                                    variant="outlined"
                                    onClick={() => history.push("/perfil")}
                                >
                                    Voltar
                                </Button>
                            </Grid>
                        </Card>
                    </Grid>
                </Grid>
                <DietsDialog dieta={this.state.dieta} open={this.state.sendDietDialog} onClose={() => this.setState({ sendDietDialog: false })} ></DietsDialog>
            </div>
        )
    }
}

export default VisualizeGoal;