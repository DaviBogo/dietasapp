import React, { Component } from "react";
import { Typography, Button, TextField, Grid, Card, Avatar, Select, FormControl, InputLabel, MenuItem } from "@material-ui/core";
import { Link } from "react-router-dom";
import history from "../../../history";
import axios from "axios";
import { Refresh } from "@material-ui/icons";

class VisualizeUser extends Component {

    state = {
        id: "",
        nome: "",
        email: "",
        sexo: "",
        peso: "",
        altura: "",
        limitacoes: ""
    }

    async componentDidMount() {
        const response =
            await
                axios.get(`http://localhost:9090/api/usuario/${localStorage.getItem("userTemporary")}`);
        this.setState({
            nome: response.data.nome,
            email: response.data.email,
            sexo: response.data.sexo,
            peso: response.data.peso,
            altura: response.data.altura,
            limitacoes: response.data.limitacoes
        })
    };

    render() {
        return (
            <div style={{ backgroundColor: "#fed766", height: "47.1em" }}>
                <Grid container justify="center">
                    <Grid item xs={12} md={4}>
                        <Card style={{ marginTop: 100, padding: 10 }}>
                            <Grid container justify="center">
                                <Grid item xs={6}>
                                    <TextField
                                        label="Nome"
                                        disabled={true}
                                        value={this.state.nome || ""}
                                        variant="outlined"
                                        fullWidth
                                        onChange={e => this.handleChange(e)}
                                        margin="dense"
                                        style={{ marginTop: 10 }} />
                                    <TextField
                                        label="Email"
                                        disabled={true}
                                        value={this.state.email || ""}
                                        variant="outlined"
                                        fullWidth
                                        onChange={e => this.handleChange(e)}
                                        margin="dense"
                                        style={{ marginTop: 10 }} />
                                    <TextField
                                        label="Sexo"
                                        disabled={true}
                                        value={this.state.sexo || ""}
                                        variant="outlined"
                                        fullWidth
                                        onChange={e => this.handleChange(e)}
                                        margin="dense"
                                        style={{ marginTop: 10 }} />
                                    <TextField
                                        label="Peso (kg)"
                                        disabled={true}
                                        value={this.state.peso || ""}
                                        variant="outlined"
                                        fullWidth
                                        onChange={e => this.handleChange(e)}
                                        margin="dense"
                                        style={{ marginTop: 10 }} />
                                    <TextField
                                        label="Altura (m)"
                                        disabled={true}
                                        value={this.state.altura || ""}
                                        variant="outlined"
                                        fullWidth
                                        onChange={e => this.handleChange(e)}
                                        margin="dense"
                                        style={{ marginTop: 10 }} />
                                    <TextField
                                        label="Limitações"
                                        disabled={true}
                                        value={this.state.limitacoes || ""}
                                        variant="outlined"
                                        fullWidth
                                        onChange={e => this.handleChange(e)}
                                        margin="dense"
                                        style={{ marginTop: 10 }} />
                                </Grid>
                                <Grid container item xs={6} style={{ marginTop: 10 }} alignItems="flex-end" justify="center">
                                    <Button
                                        color="secondary"
                                        variant="contained"
                                        disabled={false}
                                        onClick={() => history.push("/metas/visualizar")}
                                    >
                                        voltar
                                     </Button>
                                </Grid>
                            </Grid>
                        </Card>
                    </Grid>
                </Grid>
            </div >
        )
    }
}

export default VisualizeUser;