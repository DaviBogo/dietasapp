import React, { Component } from "react";
import { Typography, Button, TextField, Grid, Card, List, ListItem, ListItemText, Avatar } from "@material-ui/core";
import { Link } from "react-router-dom";
import history from "../../../history";
import axios from "axios";

class CreateGoal extends Component {

    state = {
        pesoDesejado: "",
        tempoDesejado: "",
        observacoes: ""
    }

    handleChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    createGoal = async () => {

        let usuario = {
            id: localStorage.getItem("userId")
        }
        let meta = {
            pesoDesejado: this.state.pesoDesejado,
            tempoDesejado: this.state.tempoDesejado,
            observacoes: this.state.observacoes,
            usuario: usuario
        }
        console.log(meta)
        const formData = new FormData();
        formData.set("meta", JSON.stringify(meta))
        const response =
            await
                axios.post('http://localhost:9090/api/meta', formData)
                    .then(function (response) {
                        if (response.status === 200) {
                            alert("Meta criada com sucesso!");
                        } else {
                            alert(response.message);
                        }
                    });
    };

    render() {
        return (
            <div style={{ backgroundColor: "#fed766", height: "47.1em" }}>
                <Grid container justify="center">
                    <Grid item xs={12} md={4}>
                        <Card style={{ marginTop: 100, padding: 10 }}>
                            <Typography
                                variant="h5"
                                style={{ textAlign: "center", padding: 15 }}
                            >
                                Nova Meta
                             </Typography>
                            <Grid item xs={12}>
                                <TextField
                                    label="Peso Desejado"
                                    variant="outlined"
                                    name="pesoDesejado"
                                    fullWidth
                                    onChange={e => this.handleChange(e)}
                                    style={{ marginTop: 10 }} />
                                <TextField
                                    label="Tempo Desejado"
                                    variant="outlined"
                                    name="tempoDesejado"
                                    fullWidth
                                    onChange={e => this.handleChange(e)}
                                    style={{ marginTop: 10 }} />
                                <TextField
                                    label="Observações"
                                    variant="outlined"
                                    name="observacoes"
                                    fullWidth
                                    onChange={e => this.handleChange(e)}
                                    style={{ marginTop: 10 }} />
                            </Grid>
                            <Button
                                color="secondary"
                                variant="contained"
                                fullWidth
                                onClick={() => this.createGoal()}
                                style={{ marginTop: 10, height: 50 }}
                            >
                                Criar Meta
                            </Button>
                            <Grid container justify="center">
                                <Button
                                    size="small"
                                    color="secondary"
                                    variant="outlined"
                                    onClick={() => history.push("/perfil")}
                                    style={{ marginTop: 10 }}
                                >
                                    Voltar
                            </Button>
                            </Grid>
                        </Card>
                    </Grid>
                </Grid>
            </div>
        )
    }
}

export default CreateGoal;