import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";

import VisualizeGoal from "./components/pages/VisualizeGoal"
import CreateGoal from "./components/pages/CreateGoal";
import Login from "./components/pages/Login";
import RegisterUser from "./components/pages/RegisterUser";
import UserPerfil from "./components/pages/UserPerfil";
import VisualizeUser from "./components/pages/VisualizeUser";
import VisualizeGoalsDiets from "./components/pages/VisualizeGoalsDiets"

class Routes extends Component {
  render() {
    return (
      <Switch>
        <Route exact path="/" component={Login} />
        <Route path="/usuario/cadastro" component={RegisterUser} />
        <Route path="/login" component={Login} />
        <Route path="/perfil" component={UserPerfil} />
        <Route path="/meta/criar" component={CreateGoal} />
        <Route path="/metas/visualizar" component={VisualizeGoal} />
        <Route path="/usuario/visualizar" component={VisualizeUser} />
        <Route path="/metas/dietas" component={VisualizeGoalsDiets} />
      </Switch>
    );
  }
}

export default Routes;
