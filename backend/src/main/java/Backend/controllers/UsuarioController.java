package Backend.controllers;

import Backend.exceptions.MensagemErro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import Backend.exceptions.ObjetoJaCadastradoException;
import Backend.exceptions.UsuarioInexistenteException;
import Backend.models.Usuario;
import Backend.services.UsuarioService;

import java.io.IOException;
import java.nio.file.AccessDeniedException;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api")
public class UsuarioController {

    @Autowired
    UsuarioService usuarioService;

    @PostMapping(value = "/usuario/cadastro")
    public ResponseEntity<?> cadastraUsuario(@RequestParam(value = "usuario") String usuario_texto) {

        try {
            Usuario usuario = usuarioService.cadastraUsuario(usuario_texto);
            return new ResponseEntity<>(usuario, HttpStatus.OK);
        } catch (IOException e) {
            return new ResponseEntity<>(new MensagemErro(e.getMessage()), HttpStatus.BAD_REQUEST);
        }

    }

    @PostMapping(value = "/usuario/login")
    public ResponseEntity<?> verificaLogin(
            @RequestParam(value = "email") String email,
            @RequestParam(value = "senha") String senha) {
        try {

            Usuario usuario = usuarioService.verificaLogin(email, senha);
            return new ResponseEntity<>(usuario, HttpStatus.OK);
        } catch (AccessDeniedException | UsuarioInexistenteException | NullPointerException e) {
            return new ResponseEntity<>(new MensagemErro(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/usuario")
    public ResponseEntity<?> editaUsuario(
            @RequestParam(value = "usuario") String usuario_texto) {

        try {
            Usuario usuario = usuarioService.editaUsuario(usuario_texto);
            return new ResponseEntity<>(usuario, HttpStatus.OK);
        } catch (IOException | NullPointerException e) {
            return new ResponseEntity<>(new MensagemErro(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/usuario/novasenha/{id_usuario}")
    public ResponseEntity<?> alterarSenha(
            @PathVariable(value = "id_usuario") Long idUsuario,
            @RequestParam(value = "senha_nova") String senhaNova,
            @RequestParam(value = "senha_antiga") String senhaAntiga) {

        try {
            if (idUsuario == null)
                throw new NullPointerException("O id do usuário deve ser informado");
            if (senhaNova == null)
                throw new NullPointerException("A senha nova deve ser informada");
            if (senhaAntiga == null)
                throw new NullPointerException("A senha antiga deve ser informada");

            Usuario usuario = usuarioService.buscaUsuarioPorId(idUsuario);
            usuario = usuarioService.alteraSenha(usuario, senhaNova, senhaAntiga);
            return new ResponseEntity<>(usuario, HttpStatus.OK);
        } catch (NullPointerException | UsuarioInexistenteException | AccessDeniedException e) {
            return new ResponseEntity<>(new MensagemErro(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/usuario/upgrade/nutricionista")
    public ResponseEntity<?> transformaUsuarioEmNutricionista(@RequestParam(value = "id_usuario") Long idUsuario) {

        try {
            usuarioService.transformaUsuarioEmNutricionista(idUsuario);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (UsuarioInexistenteException e) {
            return new ResponseEntity<>(new MensagemErro(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/usuario/{id_usuario}")
    public ResponseEntity<?> getUsuarioById(
            @PathVariable(value = "id_usuario") Long id) {

        try {
            return new ResponseEntity<>(usuarioService.buscaUsuarioPorId(id), HttpStatus.OK);
        } catch (UsuarioInexistenteException e) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }

    @GetMapping(value = "/nucricionista/usuarios")
    public ResponseEntity<?> getAllUsuarios(@RequestParam(value = "id_nutricionista") Long idNutricionista) {

        try {
            return new ResponseEntity<>(usuarioService.buscaTodosUsuarios(idNutricionista), HttpStatus.OK);
        } catch (IOException | UsuarioInexistenteException e) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }
}