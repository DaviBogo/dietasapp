package Backend.controllers;

import Backend.exceptions.MensagemErro;
import Backend.exceptions.UsuarioInexistenteException;
import Backend.models.Meta;
import Backend.models.Usuario;
import Backend.services.MetaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api")
public class MetaController {

    @Autowired
    MetaService metaService;

    @PostMapping(value = "/meta")
    public ResponseEntity<?> cadastraMeta(@RequestParam(value = "meta") String meta_texto) {

        try {
            Meta meta = metaService.cadastraMeta(meta_texto);
            return new ResponseEntity<>(meta, HttpStatus.OK);
        } catch (IOException e) {
            return new ResponseEntity<>(new MensagemErro(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/metas/todas")
    public ResponseEntity<?> getAllMetas() {

        try {
            List<Meta> metas = metaService.getAllMetas();
            return new ResponseEntity<>(metas, HttpStatus.OK);
        } catch (IOException | UsuarioInexistenteException e) {
            return new ResponseEntity<>(new MensagemErro(e.getMessage()), HttpStatus.BAD_REQUEST);
        }

    }

    @GetMapping(value = "/metas/usuario/{id_usuario}")
    public ResponseEntity<?> getMetasByUsuario(@PathVariable Long id_usuario) {

        try {
            List<Meta> metas = metaService.getMetasByUsuario(id_usuario);
            return new ResponseEntity<>(metas, HttpStatus.OK);
        } catch (UsuarioInexistenteException e) {
            return new ResponseEntity<>(new MensagemErro(e.getMessage()), HttpStatus.BAD_REQUEST);
        }

    }

}
