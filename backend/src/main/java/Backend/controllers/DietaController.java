package Backend.controllers;

import Backend.exceptions.MensagemErro;
import Backend.exceptions.UsuarioInexistenteException;
import Backend.models.Dieta;
import Backend.services.DietaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api")
public class DietaController {

    @Autowired
    DietaService dietaService;

    @PostMapping(value = "/dieta/cadastro")
    public ResponseEntity<?> cadastraMeta(
            @RequestParam(value = "dieta") String dieta_texto,
            @RequestParam(value = "meta") String meta_texto) {

        try {
            Dieta dieta = dietaService.cadastraDieta(dieta_texto, meta_texto);
            return new ResponseEntity<>(dieta, HttpStatus.OK);
        } catch (IOException | UsuarioInexistenteException e) {
            return new ResponseEntity<>(new MensagemErro(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

}
