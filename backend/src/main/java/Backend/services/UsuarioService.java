package Backend.services;

import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Backend.exceptions.ObjetoJaCadastradoException;
import Backend.exceptions.UsuarioInexistenteException;
import Backend.models.Usuario;
import Backend.repositories.UsuarioRepository;

@Service
public class UsuarioService {

    @Autowired
    UsuarioRepository usuarioRepository;

    @Autowired
    ObjectMapper objectMapper;

    private final String NUTRICIONISTA = "nutricionista";
    private final String USUARIO = "usuario";

    public Usuario cadastraUsuario(String usuario_texto) throws ObjetoJaCadastradoException, IOException, NullPointerException {

        Usuario usuario = objectMapper.readValue(usuario_texto, Usuario.class);

        if (usuario.getNome() == null)
            throw new NullPointerException("O nome do usuário deve ser informado.");
        if (usuario.getEmail() == null)
            throw new NullPointerException("O email do usuário deve ser informado.");
        if (usuario.getSenha() == null)
            throw new NullPointerException("A senha do usuário deve ser informado.");

        if (usuarioRepository.existsByEmail(usuario.getEmail()))
            throw new ObjetoJaCadastradoException("E-mail indisponível, tente novamente.");

        usuario.setRole(USUARIO);
        return usuarioRepository.save(usuario);
    }

    public Usuario buscaUsuarioPorId(Long idUsuario) throws UsuarioInexistenteException {

        Optional opUsuario = usuarioRepository.findById(idUsuario);
        if (opUsuario.isPresent())
            return (Usuario) opUsuario.get();
        throw new UsuarioInexistenteException("Usuario não encontrado com o id especificado.");
    }

    public Usuario verificaLogin(String email, String senha)
            throws UsuarioInexistenteException, AccessDeniedException {

        Usuario usuario = usuarioRepository.findByEmail(email);
        if (usuario == null)
            throw new UsuarioInexistenteException("Usuario não encontrado com o nome especificado.");
        if (usuario.getSenha().equals(senha))
            return usuario;
        else throw new AccessDeniedException("As informações de nome e/ou senha estão incorretas.");
    }

    public Usuario editaUsuario(String usuario_texto) throws IOException, NullPointerException {

        Usuario usuario = objectMapper.readValue(usuario_texto, Usuario.class);
        if (usuario.getNome() == null)
            throw new NullPointerException("O nome do usuário deve ser informado.");
        if (usuario.getEmail() == null)
            throw new NullPointerException("O email do usuário deve ser informado.");

        Usuario usuarioBanco = usuarioRepository.getById(usuario.getId());
        usuarioBanco.setNome(usuario.getNome());
        usuarioBanco.setEmail(usuario.getEmail());
        usuarioBanco.setSexo(usuario.getSexo());
        usuarioBanco.setPeso(usuario.getPeso());
        usuarioBanco.setAltura(usuario.getAltura());
        usuarioBanco.setLimitacoes(usuario.getLimitacoes());

        return usuarioRepository.save(usuarioBanco);
    }

    public Usuario alteraSenha(Usuario usuario, String senhaNova, String senhaAntiga) throws AccessDeniedException {

        if (senhaAntiga.equals(senhaNova))
            usuario.setSenha(senhaNova);
        else throw new AccessDeniedException("Acesso negado, senha nova deve ser igual à senha antiga.");
        usuarioRepository.save(usuario);
        return usuario;
    }

    public void transformaUsuarioEmNutricionista(Long id) throws UsuarioInexistenteException {

        Usuario usuario = buscaUsuarioPorId(id);
        usuario.setRole(NUTRICIONISTA);
        usuarioRepository.save(usuario);
    }

    public List<Usuario> buscaTodosUsuarios(Long idNutricionista) throws UsuarioInexistenteException, AccessDeniedException {

        Usuario nutricionista = buscaUsuarioPorId(idNutricionista);

        if (!nutricionista.getRole().equals(NUTRICIONISTA))
            throw new AccessDeniedException("Apenas nutricionistas podem acessar este recurso.");

        List<Usuario> listaUsuarios = new ArrayList<>();
        usuarioRepository.findAll().forEach(listaUsuarios::add);

        return listaUsuarios;
    }

    public String getNUTRICIONISTA() {
        return NUTRICIONISTA;
    }

    public String getUSUARIO() {
        return USUARIO;
    }
}
