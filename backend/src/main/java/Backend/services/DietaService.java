package Backend.services;

import Backend.exceptions.UsuarioInexistenteException;
import Backend.models.Dieta;
import Backend.models.Meta;
import Backend.repositories.DietaRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class DietaService {

    @Autowired
    DietaRepository dietaRepository;

    @Autowired
            MetaService metaService;

    ObjectMapper objectMapper = new ObjectMapper();

    public Dieta cadastraDieta(String dieta_texto, String meta_texto) throws IOException, UsuarioInexistenteException {
        Dieta dieta = objectMapper.readValue(dieta_texto, Dieta.class);
        Meta meta = objectMapper.readValue(meta_texto, Meta.class);
        meta = metaService.buscaMetaPorId(meta.getId());
        dieta.setMeta(meta);
        meta.setDieta(dieta);

        return dietaRepository.save(dieta);
    }

}
