package Backend.services;

import Backend.exceptions.UsuarioInexistenteException;
import Backend.models.Meta;
import Backend.models.Usuario;
import Backend.repositories.MetaRepository;
import Backend.repositories.UsuarioRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MetaService {

    @Autowired
    MetaRepository metaRepository;

    @Autowired
    UsuarioService usuarioService;

    ObjectMapper objectMapper = new ObjectMapper();

    public Meta cadastraMeta(String meta_texto) throws IOException {
        Meta meta = objectMapper.readValue(meta_texto, Meta.class);

        return metaRepository.save(meta);
    }

    public List<Meta> getAllMetas() throws UsuarioInexistenteException, AccessDeniedException {

        List<Meta> listaMetas = new ArrayList<>();
        metaRepository.findAll().forEach(listaMetas::add);
        return listaMetas;
    }

    public Meta buscaMetaPorId(Long idMeta) throws UsuarioInexistenteException {

        Optional opMeta = metaRepository.findById(idMeta);
        if (opMeta.isPresent())
            return (Meta) opMeta.get();
        throw new UsuarioInexistenteException("Meta não encontrada com o id especificado.");
    }

    public List<Meta> getMetasByUsuario(Long id_usuario) throws UsuarioInexistenteException {
        Usuario usuario = usuarioService.buscaUsuarioPorId(id_usuario);
        return usuario.getMetas();
    }
}
