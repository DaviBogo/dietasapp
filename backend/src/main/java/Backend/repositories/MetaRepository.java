package Backend.repositories;

import Backend.models.Meta;
import org.springframework.data.repository.CrudRepository;

public interface MetaRepository extends CrudRepository<Meta, Long> {
}
