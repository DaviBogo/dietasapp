package Backend.repositories;

import Backend.models.Dieta;
import org.springframework.data.repository.CrudRepository;

public interface DietaRepository extends CrudRepository<Dieta, Long> {

}
