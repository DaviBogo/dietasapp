package Backend.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class Meta {

    @Id
    @Column(name = "id_meta")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "peso_desejado")
    private String pesoDesejado;

    @Column(name = "tempo_desejado")
    private String tempoDesejado;

    @Column(name = "observacoes")
    private String observacoes;

    @ManyToOne()
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;

    @OneToOne()
    @JoinColumn(name = "id_dieta")
    private Dieta dieta;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPesoDesejado() {
        return pesoDesejado;
    }

    public void setPesoDesejado(String pesoDesejado) {
        this.pesoDesejado = pesoDesejado;
    }

    public String getTempoDesejado() {
        return tempoDesejado;
    }

    public void setTempoDesejado(String tempoDesejado) {
        this.tempoDesejado = tempoDesejado;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Dieta getDieta() {
        return dieta;
    }

    public void setDieta(Dieta dieta) {
        this.dieta = dieta;
    }
}
