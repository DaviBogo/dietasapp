package Backend.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Usuario {

	@Id
    @Column(name = "id_usuario")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "nome", nullable = false)
	private String nome;

	@Column(name = "email", nullable = false)
	private String email;
	
	@Column(name = "peso")
	private String peso;
	
	@Column(name = "altura")
	private String altura;
	
	@Column(name = "senha", nullable = false)
	private String senha;

	@Column(name = "role", nullable = false)
	private String role;

	@Column(name = "sexo")
	private String sexo;

	@Column(name = "limitacoes")
	private String limitacoes;

	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "usuario")
	private List<Meta> metas = new ArrayList<>();

	public Usuario() {
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPeso() {
		return peso;
	}

	public void setPeso(String peso) {
		this.peso = peso;
	}

	public String getAltura() {
		return altura;
	}

	public void setAltura(String altura) {
		this.altura = altura;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getLimitacoes() {
		return limitacoes;
	}

	public void setLimitacoes(String limitacoes) {
		this.limitacoes = limitacoes;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public void addMeta(Meta meta){
		this.metas.add(meta);
	}

	public List<Meta> getMetas() {
		return metas;
	}
}
